all: links

links:
	#zsh
	ln -s ~/.dotfiles/zsh/zdirs ~/.zdirs
	ln -s ~/.dotfiles/zsh/zcompdump ~/.zcompdump
	ln -s ~/.dotfiles/zsh/zshrc ~/.zshrc
	ln -s ~/.dotfiles/zsh/zshrc.local ~/.zshrc.local
	
	#vim
	ln -s ~/.dotfiles/vimfiles/vim ~/.vim
	ln -s ~/.dotfiles/vimfiles/vimrc ~/.vimrc


force:
	#zsh
	ln -sf ~/.dotfiles/zsh/zdirs ~/.zdirs
	ln -sf ~/.dotfiles/zsh/zcompdump ~/.zcompdump
	ln -sf ~/.dotfiles/zsh/zshrc ~/.zshrc
	ln -sf ~/.dotfiles/zsh/zshrc.local ~/.zshrc.local
	
	#vim
	ln -sf ~/.dotfiles/vimfiles/vim ~/.vim
	ln -sf ~/.dotfiles/vimfiles/vimrc ~/.vimrc
